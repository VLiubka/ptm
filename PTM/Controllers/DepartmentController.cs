﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using PTM.Models;
using System.Threading.Tasks;
using System.Data.Entity;
using Newtonsoft.Json;

namespace PTM.Controllers
{
    public class DepartmentController : Controller
    {

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Create()
        {
            //CreateDepartmentModel model = new CreateDepartmentModel();
            //using (PTMContext db = new PTMContext())
            //{
            //    model.CheckedUsers = await db.UserInfo.Where(u => u.Department == null).Select(u => new CheckedUser { Id = u.Id, Name=u.FullName, IsChecked = false }).ToListAsync();
            //}
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Create(CreateDepartmentModel model)
        {
            if (ModelState.IsValid)
            {
                List<UserInfo> users=new List<UserInfo>();
                using (PTMContext db = new PTMContext())
                {
                    //foreach (CheckedUser u in model.CheckedUsers.Where(u=>u.IsChecked==true))
                    //{
                    //    UserInfo userInfo = await db.UserInfo.FirstOrDefaultAsync(ui => ui.Id == u.Id);
                    //    if (userInfo != null)
                    //    {
                    //        users.Add(userInfo);
                    //    }
                    //}
                    Department dep = new Department { Name = model.Name, DepartmentStatus = DepartmentStatus.Active };
                    db.Departments.Add(dep);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index","Department");
                }
                
            }



            //using (PTMContext db = new PTMContext())
            //{
            //    model.CheckedUsers = await db.UserInfo.Where(u => u.Department == null).Select(u => new CheckedUser { Id = u.Id, Name = u.FullName, IsChecked = false }).ToListAsync();
            //}
            return View(model);
        }




        // GET: Department
        [Authorize]
        public ActionResult Index()
        {
            DepartmentsModel model=new DepartmentsModel();
            using (PTMContext db = new PTMContext())
            {
                //db.Departments.Add(new Department { Name = "No Department", DepartmentStatus = DepartmentStatus.Active });
                //db.Departments.Add(new Department { Name = "React", Projects = new List<Project>(), Users = new List<User>(), DepartmentStatus = DepartmentStatus.Active });

                //db.SaveChanges();
                model.Departments = db.Departments.Include(d => d.Users).Include(d=>d.Projects).ToList();
                //foreach (Department d in model.Departments)
                //{
                //    d.Users = db.Departments.ToList()[0].Users;
                //}
            }

                return View(model);
        }

        [Authorize]
        public async Task<ActionResult> Department(int id)
        {
            DepartmentModel model = new DepartmentModel();
            //Department dep;
            using (PTMContext db = new PTMContext())
            {
                model.Department = await db.Departments.Include(d => d.Users).Include(d => d.Projects).SingleOrDefaultAsync(d => d.Id == id);
                ViewBag.NoDepartmentUsersList = JsonConvert.SerializeObject(db.Users.Where(u => u.UserInfo.Department.Name == "No department").Select(u => u.UserInfo.FullName));
            }
            //model.Department = dep;
            return View(model);
        }

    }
}