﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using PTM.Models;
using PTM.Models.Identity;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Diagnostics;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PTM.Controllers
{   

    public class AccountController : Controller
    {


        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        private PTMUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<PTMUserManager>();
            }
        }








        //[AllowAnonymous]
        //public ActionResult Login()
        //{
        //    return View();
        //}

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [Authorize(Roles="Admin")]
        public ActionResult Invite()
        {
            

            var model = new InviteModel();
            List<Department> deps=null;

            using (PTMContext db = new PTMContext())
            {
                deps =  db.Departments.ToList();
            }
            
            model.Departments = deps.Select(u => new SelectListItem {  Text=u.Name, Value = u.Id.ToString()});

            return View(model);
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                PTMUser user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {
                    user = await UserManager.FindAsync(user.UserName, model.Password);
                    if (user == null)
                    {
                        ModelState.AddModelError("", "Неверный логин или пароль.");
                    }
                    else
                    {
                        ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user,
                                                DefaultAuthenticationTypes.ApplicationCookie);
                        AuthenticationManager.SignOut();
                        AuthenticationManager.SignIn(new AuthenticationProperties
                        {
                            IsPersistent = true
                        }, claim);
                        if (String.IsNullOrEmpty(returnUrl))
                            return RedirectToAction("Index", "Home");
                        return Redirect(returnUrl);
                    }
                }
            }
            ViewBag.returnUrl = returnUrl;
            return View(model);
        }


        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        //if (ModelState.IsValid)
        //{
        //    // поиск пользователя в бд
        //    User user = null;
        //    using (MainContext db = new MainContext())
        //    {
        //        user = await db.Users.FirstOrDefaultAsync(u => u.Email == model.Email && u.Password == model.Password);
        //    }
        //    if (user != null)
        //    {
        //        if (user.UserStatus != UserStatus.Active)
        //        {
        //            ModelState.AddModelError("", "Пользователь не активен");
        //        }
        //        else
        //        {
        //            FormsAuthentication.SetAuthCookie(model.Email, true);
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        ModelState.AddModelError("", "Пользователя с таким логином и паролем нет");
        //    }
        //}           
        //return View(model);
        //}
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="Admin")]
        public async Task<ActionResult> Invite(InviteModel model)
        {           
            if (ModelState.IsValid)
            {
                int DepartmentId = model.Department;
                string Password = GeneratePassword();
                Department dep;


                if (await UserManager.FindByEmailAsync(model.Email) != null)
                {
                    ModelState.AddModelError("", "Email used already!");
                }
                else
                {
                    PTMUser user;
                    UserInfo userInfo;
                    using (PTMContext db = new PTMContext())
                    {

                        userInfo = new UserInfo { FullName = model.Login, UserStatus = UserStatus.NotActivated };
                        user = new PTMUser { UserName = model.Login, Email = model.Email, UserInfo = userInfo };
                    }
                    IdentityResult result = await UserManager.CreateAsync(user, Password);
                    if (result.Succeeded)
                    {
                        
                        result = await UserManager.AddToRoleAsync(user.Id, model.Role.ToString());

                        using (PTMContext db = new PTMContext())
                        {
                            dep = await db.Departments.FindAsync(DepartmentId);
                            dep.Users.Add(db.Users.Include(u => u.UserInfo).FirstOrDefault(u => u.Id == user.Id).UserInfo);

                            //user = await db.Users.Include(u => u.UserInfo).FirstOrDefaultAsync(u => u.Id == user.Id);
                            //user.UserInfo.Department = dep;
                            await db.SaveChangesAsync();
                        }
                        //dep.Users.Add(user.UserInfo);
                        //user.UserInfo.Department.Users.Add(user);
                        //using (PTMContext db = new PTMContext())
                        //{
                        //    dep = await db.Departments.FirstOrDefaultAsync(u => u.Id == DepartmentId);                            
                        //    await db.SaveChangesAsync();
                        //}
                        if (!result.Succeeded)
                            {
                                //result = await UserManager.AddToRoleAsync(user.Id, "No Role");
                            }
                       
                            return RedirectToAction("Login", "Account");

                    }
                    else
                    {
                        ModelState.AddModelError("", "Login used already!");
                    }
                }
            }
            List<Department> deps = null;

            using (PTMContext db = new PTMContext())
            {
                deps = db.Departments.ToList();
            }

            model.Departments = deps.Select(u => new SelectListItem { Text = u.Name, Value = u.Id.ToString() });
            return View(model);
            }



        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Profile()
        {
            ProfileModel model = new ProfileModel();
            using (PTMContext db = new PTMContext())
            {
                string id = User.Identity.GetUserId();
                model.User = await db.Users.Include(u=>u.UserInfo.Department).Include(u=>u.UserInfo.Projects).Include(u=>u.UserInfo.Tasks).FirstOrDefaultAsync(u => u.Id == id);   
                //UserManager.FindByIdAsync(User.Identity.GetUserId());
            }
            return View(model);
        }

            //    User user = null;
            //    using (MainContext db = new MainContext())
            //    {
            //        user = await db.Users.FirstOrDefaultAsync(u => u.Email == model.Email || u.Login==model.Login);
            //    }
            //    if (user == null)
            //    {
            //        // создаем нового пользователя
            //        string Password = GeneratePassword();
            //        int DepartmentId = model.Department;
            //        using (MainContext db = new MainContext())
            //        {
            //            var dep = await db.Departments.FirstOrDefaultAsync(u => u.Id == DepartmentId);
            //            db.Users.Add(new User { Email = model.Email, Password = Password, Login=model.Login, Department=dep, Role=model.Role, UserStatus=UserStatus.NotActivated });
            //            await db.SaveChangesAsync();
            //        }
            //        return RedirectToAction("Login", "Account");
            //    }
            //    else
            //    {
            //        ModelState.AddModelError("", "Пользователь с таким логином уже существует");
            //    }
            //}
            ////model.Department =0;     //need fix 
            //List<Department> deps = null;

            //using (MainContext db = new MainContext())
            //{
            //    deps = db.Departments.ToList();
            //}

            //model.Departments = deps.Select(u => new SelectListItem { Text = u.Name, Value = u.Id.ToString() });
            //return View(model);
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult Login(LoginModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        // поиск пользователя в бд
        //        User user = null;
        //        using (MainContext db = new MainContext())
        //        {
        //            user = db.Users.FirstOrDefault(u => u.Email == model.Email && u.Password == model.Password);
        //        }
        //        if (user != null)
        //        {
        //            FormsAuthentication.SetAuthCookie(model.Email, true);
        //            return RedirectToAction("Index", "Home");
        //        }
        //        else
        //        {
        //            //ModelState.AddModelError("", "Пользователя с таким логином и паролем нет");
        //        }
        //    }

        //    return View(model);
        //}

        public string GeneratePassword()
        {
            return Membership.GeneratePassword(16, 0);
        }

//        [HttpPost]
//        [AllowAnonymous]
//        public  JsonResult CheckEmail(string email)
//        {
//            var result = 0;
//            using (MainContext db = new MainContext())
//            {
//                result = db.Users.Count(u=>u.Email == email);
//            }
//            return Json(result==0, JsonRequestBehavior.AllowGet);
//        }

//        [AllowAnonymous]
//        [HttpGet]
//        public async Task<JsonResult> CheckLogin(string login)
//        {
//            Debug.WriteLine("Check login");

//            var result = 0;
//            using (MainContext db = new MainContext())
//            {
//                result = await db.Users.CountAsync(u => u.Login == login);
//}
//            return Json(result==0, JsonRequestBehavior.AllowGet);
//        }
        
    }
}