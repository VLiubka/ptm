﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using PTM.Models;
using System.Threading.Tasks;
using System.Data.Entity;
using PagedList;
using PagedList.Mvc;

namespace PTM.Controllers
{
    public class ProjectController : Controller
    {
        // GET: Project
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Create()
        {
            CreateProjectModel model=new CreateProjectModel();
            using (PTMContext db = new PTMContext())
            {
                model.CheckedDepartments = await db.Departments.Select(d => new CheckedDepartment { Id = d.Id, Name = d.Name, UsersCount=d.Users.Count, IsChecked = false }).ToListAsync();
                //for (int i = 0; i < model.CheckedDepartments.Count; i++)
                //{
                //    int id = model.CheckedDepartments[i].Id;
                //    model.CheckedDepartments[i].CheckedUsers = await db.UserInfo.Where(u => u.Department.Id == id).Select(u => new CheckedUser { Id = u.Id, Name = u.FullName, IsChecked = false }).ToListAsync();
                //    //   model.CheckedUsers.Add(db.UserInfo.Where(u => u.Department.Id == id).ToList().Select(u => new CheckedUser { Id = u.Id, Name = u.FullName, IsChecked = false }).ToList());
                //}
                //var CheckedUsers=await db.UserInfo.Select(u => new CheckedUser { Id = u.Id, Name = u.FullName, IsChecked = false }).ToListAsync();
            }
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Create(CreateProjectModel model)
        {




            using (PTMContext db = new PTMContext())
            {
                model.CheckedDepartments = await db.Departments.Select(d => new CheckedDepartment { Id = d.Id, Name = d.Name, UsersCount = d.Users.Count, IsChecked = false }).ToListAsync();
                for (int i = 0; i < model.CheckedDepartments.Count; i++)
                {
                    model.CheckedDepartments[i].CheckedUsers = db.UserInfo.Where(u => u.Department.Id == model.CheckedDepartments[i].Id).ToList().Select(u => new CheckedUser { Id = u.Id, Name = u.FullName, IsChecked = false }).ToList();
                    //modelCheckedUsers.Add(db.UserInfo.Where(u => u.Department.Id == model.CheckedDepartments[i].Id).ToList().Select(u => new CheckedUser { Id = u.Id, Name = u.FullName, IsChecked = false }).ToList());
                }
            }
            return View(model);
        }


        [Authorize]
        public async Task<ActionResult> Index()
        {

            ProjectsModel model = new ProjectsModel();
            using (PTMContext db = new PTMContext())
            {

                //db.Departments.Add(new Department { Name = "No Department", DepartmentStatus = DepartmentStatus.Active });
                //db.Projects.Add(new Project { Name = "TestProject_1", ProjectStatus = ProjectStatus.NotStarted });
                //db.Projects.Add(new Project { Name = "TestProject_2", ProjectStatus = ProjectStatus.Completed });
                //db.SaveChanges();
                //List<Project> projects = await db.Projects.ToListAsync();
                //projects[0].Users.Add(db.Users.FirstOrDefault(u => u.UserName == "2"));
                //db.Projects.Add(new Project { Name = "TestProject_3", ProjectStatus = ProjectStatus.Failed });
                //db.Projects.Add(new Project { Name = "TestProject_4", ProjectStatus = ProjectStatus.Active });


                //db.SaveChanges();
                //Project pr = db.Projects.ToList()[1];
                //pr.Departments.Add(db.Departments.ToList()[1]);
                //pr.Users.Add(db.Users.ToList()[1]);
                //db.SaveChanges();
                model.Projects =await db.Projects.Include(p => p.Users).Include(p => p.Departments).ToListAsync();

                //foreach (Department d in model.Departments)
                //{
                //    d.Users = db.Departments.ToList()[0].Users;
                //}
            }
            return View(model);
        }


        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Project(int id)
        {
            ProjectModel model = new ProjectModel();
            using (PTMContext db = new PTMContext())
            {
                var pr = await db.Projects.Include(p => p.Tasks).Include(p=>p.Departments).Include(p=>p.Users).FirstOrDefaultAsync(p => p.Id == id);
                model.Project = pr;
            }
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> GetTasks(int id)
        {
            List<Models.Task> Tasks=new List<Models.Task>();
            Tasks model=new Tasks();

            using (PTMContext db=new PTMContext())
            {
                var pr = await db.Projects.Include(p=>p.Tasks).FirstOrDefaultAsync(p=>p.Id==id);
                pr.Tasks.Add(new Models.Task {Name="Task1", TaskStatus=Models.TaskStatus.Submitted, IsBug=true });
                pr.Tasks.Add(new Models.Task { Name = "Task2", TaskStatus = Models.TaskStatus.Submitted });
                pr.Tasks.Add(new Models.Task { Name = "Task3", TaskStatus = Models.TaskStatus.Submitted });
                pr.Tasks.Add(new Models.Task { Name = "Task4", TaskStatus = Models.TaskStatus.QA });
                Tasks = pr.Tasks.ToList();
            }
            model.Submitted = Tasks.Where(t => t.TaskStatus == Models.TaskStatus.Submitted).ToList();
            model.InProgress = Tasks.Where(t => t.TaskStatus == Models.TaskStatus.InProgress).ToList();
            model.QA = Tasks.Where(t => t.TaskStatus == Models.TaskStatus.QA).ToList();
            model.Repeated = Tasks.Where(t => t.TaskStatus == Models.TaskStatus.Repeated).ToList();
            model.Discussed = Tasks.Where(t => t.TaskStatus == Models.TaskStatus.Discussed).ToList();
            model.Completed = Tasks.Where(t => t.TaskStatus == Models.TaskStatus.Completed).ToList();
            return PartialView(model);
        }
    }
}