﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using PTM.Models;
using System.Threading.Tasks;
using System.Data.Entity;
using Newtonsoft.Json;
using PTM.Models.WorkerModels;
using PagedList;
using PagedList.Mvc;

namespace PTM.Controllers
{
    public class WorkerController : Controller
    {
        //[HttpGet]
        //[Authorize]
        //public async Task<ActionResult> Index()
        //{
        //    List<UserInfo> query;
        //    using (PTMContext db = new PTMContext())
        //    {
        //        query = await db.UserInfo.Include(u => u.Projects).Include(u=>u.Department).ToListAsync();
        //    }
        //    List<WorkersModel> resQuery = query.Select(u => new WorkersModel { N = query.IndexOf(u)+1, Name = u.FullName, Department =(u.Department!=null)?u.Department.Name:"", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).OrderBy(u=>u.N).ToList();
        //    return View(resQuery.ToPagedList(1, 20));
        //}

        //[HttpGet]
        //[Authorize]
        //public async Task<ActionResult> GetWorkers(string sortOrder, string q, int page = 1, int pageSize = 20)
        //{
        //    q = String.IsNullOrEmpty(q) ? "" : q;
        //    ViewBag.CurrentFilter = q;

        //    page = page > 0 ? page : 1;
        //    pageSize = pageSize > 0 ? pageSize : 20;

        //    ViewBag.CurrentPageSize = pageSize;
        //    ViewBag.CurrentSort = sortOrder;

        //    List<UserInfo> query;
        //    using (PTMContext db = new PTMContext())
        //    {
        //        query = await db.UserInfo.Include(u => u.Projects).OrderBy(u => u.FullName).Include(u => u.Department).Where(u => u.FullName.Contains(q)).ToListAsync();
        //    }
        //    List<WorkersModel> resQuery;

        //    switch (sortOrder)
        //    {
        //        case "n":
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u => u.Id).ToList();
        //            break;
        //        case "name":
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u => u.Name).ToList();
        //            break;
        //        case "department":
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u => u.Department).ToList();
        //            break;
        //        case "project":
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u => u.ProjectsCount).ToList();
        //            break;
        //        case "status":
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u => u.UserStatus).ToList();
        //            break;


        //        case "n_desc":
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderByDescending(u => u.Id).ToList();
        //            break;
        //        case "name_desc":
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderByDescending(u => u.Name).ToList();
        //            break;
        //        case "department_desc":
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderByDescending(u => u.Department).ToList();
        //            break;
        //        case "project_desc":
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderByDescending(u => u.ProjectsCount).ToList();
        //            break;
        //        case "status_desc":
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderByDescending(u => u.UserStatus).ToList();
        //            break;

        //        default:
        //            resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u => u.Id).ToList();
        //            break;

        //    }




        //    return PartialView(resQuery.ToPagedList(page, pageSize));
        //}

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Index(string sortOrder, string q, int page = 1, int pageSize = 20)
        {
            q = String.IsNullOrEmpty(q) ? "" : q;
            ViewBag.CurrentFilter =  q;

            page = page > 0 ? page : 1;
            pageSize = pageSize > 0 ? pageSize : 20;

            ViewBag.CurrentPageSize = pageSize;
            ViewBag.CurrentSort = sortOrder;

            List<UserInfo> query;
            using (PTMContext db = new PTMContext())
            {
                query = await db.UserInfo.Include(u => u.Projects).OrderBy(u => u.FullName).Include(u=>u.Department).Where(u=>u.FullName.Contains(q)).ToListAsync();
            }
            List<WorkersModel> resQuery;
               
                switch (sortOrder)
                {
                case "n":
                    resQuery = query.Select(u => new WorkersModel { Id=u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u=>u.Name.Contains(q)).OrderBy(u => u.Id).ToList();
                    break;
                case "name":
                        resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u=>u.Name).ToList();
                    break;
                case "department":
                    resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u => u.Department).ToList();
                    break;
                case "project":
                    resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u => u.ProjectsCount).ToList();
                    break;
                case "status":
                    resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u => u.UserStatus).ToList();
                    break;


                case "n_desc":
                    resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderByDescending(u => u.Id).ToList();
                    break;
                case "name_desc":
                    resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderByDescending(u => u.Name).ToList();
                    break;
                case "department_desc":
                    resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderByDescending(u => u.Department).ToList();
                    break;
                case "project_desc":
                    resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderByDescending(u => u.ProjectsCount).ToList();
                    break;
                case "status_desc":
                    resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderByDescending(u => u.UserStatus).ToList();
                    break;

                default:
                        resQuery = query.Select(u => new WorkersModel { Id = u.Id, Name = u.FullName, Department = (u.Department != null) ? u.Department.Name : "", ProjectsCount = u.Projects.Count, UserStatus = u.UserStatus }).Where(u => u.Name.Contains(q)).OrderBy(u=>u.Id).ToList();
                    break;

                }
            
             
                
                        
            return View(resQuery.ToPagedList(page, pageSize));
        }


    }
}