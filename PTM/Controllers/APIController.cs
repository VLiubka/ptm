﻿using PTM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;



public class treeDepartment
{
    public int id { get; set; }

    public string text { get; set; }

    public int count { get; set; }

    public bool @checked { get; set; }

    public bool hasChildren { get; set; }

    public List<treeWorker> children { get; set; }
}

public class treeWorker
{
    public int ID { get; set; }
    public string text { get; set; }
    public bool @checked { get; set; }
    public bool IsActive { get; set; }
}



namespace PTM.Controllers
{
    public class APIController : Controller
    {
        // GET: API
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> GetWorkers()
        {
            List<treeDepartment> data;
            List<Department> deps;
            using (PTMContext db = new PTMContext())
            {
                deps = await db.Departments.Include(d => d.Users).ToListAsync();
            }
            data = deps.Select(d => new treeDepartment
            {
                id = d.Id,
                text = d.Name,
                count = d.Users.Count,
                children = d.Users.Select(u => new treeWorker { ID = u.Id, text = u.FullName }).ToList()
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);

        }
        [Authorize]
        [HttpGet]
        public async Task<ActionResult> NoDepartmentUsers()
        {
            List<treeDepartment> data;
            List<Department> deps;
            using (PTMContext db = new PTMContext())
            {
                deps = await db.Departments.Include(d => d.Users).ToListAsync();
            }
            data = deps.Select(d => new treeDepartment
            {
                id = d.Id,
                text = d.Name,
                count = d.Users.Count,
                children = d.Users.Select(u => new treeWorker { ID = u.Id, text = u.FullName }).ToList()
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);

        }


    }
    
}