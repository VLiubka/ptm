﻿
function renderTable(list) {
    var NoDepartmentUsers = JSON.parse(list);

    // Create a jqxListBox
    $("#jqxListBox").jqxListBox({
        source: NoDepartmentUsers,
        width: '250px',
        height: '200px',
        theme: 'energyblue',
        checkboxes: true
    });

    

    $('#btnAdd').on('click', function () {
        $('#ID').val('');
        $("#NDUListBox").NDUListBox('uncheckAll');
        $('#dialog').dialog('open');
    });
    $('#btnSave').click(function () {
        var items = $("#NDUListBox").jqxListBox('getCheckedItems');
        var checkedItems = "";

        $.each(items, function (index) {
            if (index < items.length - 1) {
                checkedItems += this.value + ",";
            }
            else checkedItems += this.value;
        });

        var Users = { ID: $('#ID').val(), Users: checkedItems }

        $.ajax({
            type: "POST",
            url: "/Department/AddUsers",
            data: JSON.stringify(Users),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: UpdateUsersList
        });
        dialog.close();
    });
    $('#btnCancel').on('click', function () {
        dialog.close();
    });
};