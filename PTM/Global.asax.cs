﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PTM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PTM
{
    public class MvcApplication : System.Web.HttpApplication
    {
        


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            try
            {
                AddRoles();
                AddNoDepartment();
            }
            catch
            { }
        }

        public void AddNoDepartment()
        {
            using (PTMContext db = new PTMContext())
            {
                Department dep = null;
                dep = db.Departments.FirstOrDefault(d=>d.Name == "No department");
                if (dep == null)
                {
                    db.Departments.Add(new Department { Name = "No department", DepartmentStatus = DepartmentStatus.Active });
                    db.SaveChanges();
                }
            }
        }

        public void AddRoles()
        {
            PTMContext db = new PTMContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists("Master"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Master";
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists("Worker"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Worker";
                roleManager.Create(role);
            }
        }
}
}
