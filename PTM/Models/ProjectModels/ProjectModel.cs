﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTM.Models
{
    public class ProjectsModel
    {
        public List<Project> Projects { get; set; }
    }

    public class ProjectModel
    {
        public Project Project { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public List<Department> Departments { get; set; }
        ////public List<UserInfo> Users { get; set; }
        //public List<Task> Tasks { get; set; }
    }



    public class Tasks
    {
        public List<Task> Submitted { get; set; }
        public List<Task> InProgress { get; set; }
        public List<Task> QA { get; set; }
        public List<Task> Repeated { get; set; }
        public List<Task> Discussed { get; set; }
        public List<Task> Completed { get; set; }
    }

    public class CreateProjectModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<CheckedDepartment> CheckedDepartments { get; set; }
    }

    public class CheckedDepartment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UsersCount { get; set; }
        public List<CheckedUser> CheckedUsers { get; set; }
        public bool IsChecked { get; set; }
    }
    public class CheckedUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }
    }
}