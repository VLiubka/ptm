﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PTM.Models
{



    public class PTMContext : IdentityDbContext<PTMUser>
    {
        public PTMContext() : base("PTMdb") { }

        public static PTMContext Create()
        {
            return new PTMContext();
        }

        public DbSet<UserInfo> UserInfo
        {
            get; set;
        }

        public DbSet<Department> Departments
        {
            get; set;
        }

        public DbSet<Project> Projects
        {
            get; set;
        }

        public DbSet<Task> Tasks
        {
            get; set;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer<PTMContext>(null);
            modelBuilder.Entity<UserInfo>().HasOptional(u => u.Department).WithMany(d => d.Users);
            modelBuilder.Entity<UserInfo>().HasMany(u => u.Projects).WithMany(p => p.Users);
            modelBuilder.Entity<UserInfo>().HasMany(u => u.Tasks).WithMany(t => t.Users);

            //modelBuilder.Entity<Department>().HasMany(u => u.Users);
            //modelBuilder.Entity<Project>().HasMany(u => u.Users);
            //modelBuilder.Entity<Project>().HasMany(p => p.Tasks);
            //modelBuilder.Entity<Task>().HasMany(u => u.Users);

            modelBuilder.Entity<Department>().HasMany(d => d.Projects).WithMany(p => p.Departments);



            base.OnModelCreating(modelBuilder);
        }


    }




    //public class MainContext : DbContext
    //{
    //    public MainContext() : base("MainDatabase") { }

    //    public DbSet<User> Users
    //    {
    //        get; set;
    //    }

    //    public DbSet<Department> Departments 
    //    {
    //        get; set;
    //    }

    //    public DbSet<Project> Projects
    //    {
    //        get; set;
    //    }

    //    public DbSet<Task> Tasks
    //    {
    //        get; set;
    //    }
    //}

    //public class DepartmentContext : DbContext
    //{
    //    public DepartmentContext() : base("MainContext") { }

    //    public DbSet<Department> Departments
    //    {
    //        get; set;
    //    }
    //}

    //public class ProjectContext : DbContext
    //{
    //    public ProjectContext() : base("MainContext") { }

    //    public DbSet<Project> Projects
    //    {
    //        get; set;
    //    }
    //}

    //public class TaskContext : DbContext
    //{
    //    public TaskContext() : base("MainContext") { }

    //    public DbSet<Task> Tasks
    //    {
    //        get; set;
    //    }
    //}
}