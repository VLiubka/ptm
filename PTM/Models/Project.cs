﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PTM.Models
{
    public class Project
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Department> Departments { get; set; }
        public virtual ICollection<UserInfo> Users { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        //public Task[] Tasks { get; set; }
        public ProjectStatus ProjectStatus { get; set; }
    }
}