﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PTM.Models
{
    public class Department
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        //public List<User> Users { get; set; }
        public virtual ICollection<UserInfo> Users { get; set; }
        //public List<Project> Projects { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
        public DepartmentStatus DepartmentStatus { get; set; }
    }
}