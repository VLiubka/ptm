﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTM.Models
{
    public enum TaskStatus
    {
        Submitted,
        InProgress,

        QA,
        Repeated,
        Discussed,
        Completed
    }
}