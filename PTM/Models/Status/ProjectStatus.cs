﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTM.Models
{
    public enum ProjectStatus
    {
        NotStarted,
        Active,
        Completed,
        Failed
    }
}