﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTM.Models
{
    public class LoginModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(50)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, MinimumLength =8)]
        public string Password { get; set; }
    }

    public class InviteModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(50)]
        [Remote("CheckEmail", "Account")]
        public string Email { get; set; }
        
        [Required]
        [StringLength(100)]
        [Remote("CheckLogin", "Account",HttpMethod ="Post", ErrorMessage = "Login already taken!")]
        public string Login { get; set; }

        [Required]          
        public Role Role { get; set; }

        [Required]
        //[UIHint("Collection")]
        public int Department { get; set; }


        public IEnumerable<SelectListItem> Departments { get; set; }
    }

    public class ProfileModel
    {
        public PTMUser User { get; set; }
    }

}