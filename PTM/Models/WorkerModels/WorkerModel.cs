﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTM.Models.WorkerModels
{
    public class WorkersModel
    {
        //public List<UserInfo> Workers { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public int ProjectsCount { get; set; }
        public string Contact { get; set; }
        public UserStatus UserStatus { get; set; }
    }
}