﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace PTM.Models.Identity
{
    public class PTMUserManager:UserManager<PTMUser>
    {
        public PTMUserManager(IUserStore<PTMUser> store) 
            : base(store) 
        {
        }

        public static PTMUserManager Create(IdentityFactoryOptions<PTMUserManager> options,
                                                IOwinContext context)
        {
            PTMContext db = context.Get<PTMContext>();
            PTMUserManager manager = new PTMUserManager(new UserStore<PTMUser>(db));
            return manager;
        }

    }


}