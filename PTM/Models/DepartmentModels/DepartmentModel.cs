﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTM.Models
{
    public class DepartmentsModel
    {
        public List<Department> Departments { get; set; }
    }

    public class DepartmentModel
    {
        public Department Department { get; set; }
    }

    public class CreateDepartmentModel
    {
        public string Name { get; set; }
        //public List<CheckedUser> CheckedUsers { get; set; }
    }


//    public class CheckedUser
//    {
//        public int Id { get; set; }
//        public string Name { get; set; }
//        public bool IsChecked { get; set; }
//    }
}