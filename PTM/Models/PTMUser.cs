﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PTM.Models
{
    public class PTMUser : IdentityUser
    {
        public UserInfo UserInfo { get; set; }

        public PTMUser()
        {
        }
    }
}