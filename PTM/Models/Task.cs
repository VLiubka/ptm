﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PTM.Models
{
    public class Task
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Department Department { get; set; }
        public virtual ICollection<UserInfo> Users { get; set; }
        public bool IsBug { get; set; }
        public DateTime Deadline { get; set; }
        public double AllottedTime { get; set; }
        public TaskStatus TaskStatus { get; set; }
    }
}